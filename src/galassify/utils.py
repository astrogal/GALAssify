"""
Utilities (:mod:`galassify.utils`)
==================================

This module contains various utility functions and parameters.
"""
import os
import re
import glob
import argparse
from pathlib import Path
import warnings
from typing import Union

import pandas as pd
from PyConsoleMenu import SelectorMenu

try:
    # Using pkg_resources (deprecated in python 3.12)
    import pkg_resources
    module_path = pkg_resources.resource_filename(__name__, '')
except ImportError:
    # Using importlib_resources python 3.12+
    from importlib.resources import files as irfiles
    module_path = irfiles(__name__)

### OPTION PARSER UTILS

args = None
VERSION = ''
importData = pd.DataFrame()
groups = pd.DataFrame()


def getOptions(version: str) -> argparse.Namespace:
    """
    Parse the arguments given by the user.

    If any error is detected, or -h / --help is asked, a help guide is displayed and the program exits.

    Returns
    -------
    Namespace
        Returns parsed command-line arguments.
    """
    global args
    global VERSION

    parser = argparse.ArgumentParser(prog='GALAssify', description="GALAssify: Tool to manually classify galaxies.")
    parser.add_argument('--version', action='version', version='%(prog)s ' + version,
                        help="Prints software version and exit.\n")

    parser.add_argument('--init',action='store_true',help='Generate a basic config file, and the basic folder structure.')

    parser.add_argument('--example', type=example_type, default='', help='Run a galassify (basic or fits) example.')

    parser.add_argument('-p', '--path', type=dir_path, default='img/',
                        help="Path to image files.\n")

    parser.add_argument('-c', '--config', type=dir_file, default='config',
                        help="Config json file to load.\n")

    #parser.add_argument('-f', '--filename', nargs='+',
    #                    required=not('-p' in sys.argv
    #                    or '--path' in sys.argv
    #                    or '--version' in sys.argv), type=dir_file,
    #                    help="Image or list to classify. Not required if path or path + group are given.\n")

    parser.add_argument('-s', '--savefile',
                        #required=not('--version' in sys.argv),
                        default='output.csv',
                        help="CSV file to load and export changes. If does not exists, a new one is created.\n")
    parser.add_argument("-l", "--list", action="store_true",
                        help="List selected files only and exit.\n")

    parser.add_argument('-i', '--inputfile', type=dir_file, default='files/galaxies.csv',
                        help="""Galaxy database file in *.csv format.
                        Minimum required columns: ['galaxy'].
                        Recomended columns: ['group', 'galaxy', 'ra', 'dec', 'filename' and/or 'fits']""")

    parser.add_argument('group', metavar='GROUP', nargs='*',
                        help="Group number. Selects images with name format: img_<group>_*.png\n")

    args = parser.parse_args()
    VERSION = version
    return args

def exist_basic_files():
    """
    Checks if the basic needed files and folders exists.

    Returns
    -------
    bool
        Returns True if the basic files exists. Else, False ir returned.
    """
    files = ['files/','img/','config']
    test_files = True
    for file in files:
        if not Path(file).exists():
            test_files = False
    if not Path('files/galaxies.csv').exists():
        test_files = False
    return test_files

def load_menu() -> int:
    """
    Command-line menu to initialize the tool.

    Gives the user an easy way to initialize the tool with the following options:
    - INIT
    - EXAMPLE: BASIC or FITS


    Returns
    -------
    int
        Selected option (0 - INIT, 10 - EXAMPLE_BASIC, 11 - EXAMPLE_FITS , 3 - EXIT)
    """
    select_option = 3

    options = ["INIT (--init):\tGenerate a basic config file, and the basic folder structure.",
               "EXAMPLE (--example):\tGenerate a files with a galassify example",
               "EXIT"]
    menu = SelectorMenu(options, title='GALAssify: A tool to manually classify galaxies.')
    ans = menu.input()
    if ans.index==0:
        select_option = 0
    elif ans.index==1:
        options_examples = ["BASIC (--example basic):\tLoad the images example",
                            "FITS (--example fits):\tLoad the images fits example",
                            "BACK"]
        menu_examples = SelectorMenu(options_examples,
                              title='GALAssify Example: Generate a files whit a galassify example')
        ans_examples = menu_examples.input()
        if (ans_examples.index==0):
            select_option = 10
        elif (ans_examples.index==1):
            select_option = 11
        else:
            load_menu()
    return select_option

def init_flag():
    """
    Generates a basic initial files and folders in the user selected directory.
    """
    print("INFO:\tGenerating the necessary files...")
    try:
        os.system(f"cp '{getPackageResource('config')}' config")
        os.system("mkdir -p files img/fits")
        os.system('echo "group,galaxy,ra,dec,filename,fits" >> files/galaxies.csv')
        print("INFO:\tThe necessary files were created.")
        print("INFO:\tNow, complete de input files and open GALAssify with: galassify -i files/galaxies.csv -s files/output.csv -p img/ ")
    except:
        pass

def example_flag(type_example):
    """
    Copies example initial files and folders in the user selected directory.
    """
    print("INFO:\tGenerating the necessary files...")
    try:
        # Copy common files
        os.system(f"cp '{getPackageResource('config')}' config")
        os.system("mkdir -p files img")

        # Copy and create example-specific files
        if type_example == 'basic':
            os.system(f"cp '{getPackageResource('files/galaxies.csv')}' files/galaxies.csv")
            content = glob.glob(f"{getPackageResource('img/')}")
            content = [f for f in os.listdir(f"{getPackageResource('img/')}") if re.match(r'.*\.jpeg', f)]
            for element in content:
                elemet_dir = getPackageResource('img/'+element)
                os.system(f"cp '{elemet_dir}' img/{element}")

        else:
            os.system(f"cp '{getPackageResource('files/galaxies_fits.csv')}' files/galaxies.csv")
            os.system("mkdir -p img/fits")
            content=glob.glob(f"{getPackageResource('img/fits/')}")
            content = [f for f in os.listdir(f"{getPackageResource('img/fits/')}") if re.match(r'.*\.fits', f)]
            for element in content:
                elemet_dir = getPackageResource('img/fits/'+element)
                os.system(f"cp '{elemet_dir}' img/fits/{element}")

        print("INFO:\tThe necessary files were created.")
        print("INFO:\tNow, open GALAssify with: galassify -i files/galaxies.csv -s files/output.csv -p img/ ")
        print("INFO:\tIf needed, you can download catalogue images by executing: get_images_sdss -i files/galaxies.csv -p img/ ")
    except Exception as e:
        print(f"ERROR: Error while creating the necessary files: {e}")

def getVersion():
    """
    Gets the current GALAssify version.

    Returns
    -------
    str
        Current GALAssify version.
    """
    global VERSION
    return VERSION

def getPackageResource(relative_path) -> str:
    """
    Get the absolute path to a resource file.
    """
    global module_path

    return os.path.join(module_path, relative_path)

def getFiles() -> tuple:
    """
    Reads the input arguments and manages the groups given by the user and
    the input files.

    Returns
    -------
    pandas.DataFrame
        Dataframe containing all the files selected by the user.

    list
        List containing all the groups selected by the user.
    """
    global args
    global groups
    # files = []
    selectedGroups = []
    selectedFiles = pd.DataFrame(columns = groups.columns)

    imgpath = args.path

    if imgpath:
        fname = args.inputfile
        file = Path(fname)
        if file.is_file():
            groups = readInputFile(fname)
        else:
            groups = createInputFile(imgpath, fname)

        availableGroups = None
        if 'group' in groups.columns:
            availableGroups = groups.group.unique()
            print(f"INFO:\tAvailable groups: {str(availableGroups)}")

            inputGroups = args.group
            if len(inputGroups) > 0:
                for group in inputGroups:
                    if group in availableGroups:
                        selectedFiles = pd_concat(selectedFiles, groups[groups.group == group])
                        selectedGroups.append(group)
                    else:
                        print(f'WARNING:\tGroup {group} not available.')
            else:
                print('INFO:\tNo group selected. Using all available by default.')
                selectedFiles = groups.copy()
                selectedGroups = availableGroups
        else:
            print('INFO:\tNo group in input file. Using all galaxies by default.')
            selectedFiles = groups.copy()
            selectedGroups = availableGroups

        #else:
        #    formats = ['*.png']

        #for format in formats:
        #    for entry in Path(args.path).glob(format):
        #        files.append(entry)

    #elif args.filename:
    #    for entry in args.filename:
    #        files.append(Path(entry))

    #else:
    #    files = []
    return selectedFiles, selectedGroups

def example_type(type_text):
    """
    Simple parser of the example selected by the user.

    Parameters
    ----------
    type_text : str
        Type of the selected example. Can be 'basic', 'fits' or be empty.

    Raises
    ------
    argparse.ArgumentTypeError
        Raised when no valid example type was given.
    """
    if type_text not in ['basic', 'fits', '']:
        raise argparse.ArgumentTypeError(f"readable_example_type: {type_text} is not a valid example type.")
    return type_text

def dir_path(path):
    """
    Argument parser for --path option.

    Parameters
    ----------
    path : str
        Path to save image files.

    Returns
    -------
    pathlib.Path
        Path object of the found folder indicated by the user.

    Raises
    ------
    argparse.ArgumentTypeError
        Raised when the indicated path is not readable nor reachable.
    """
    d = Path(path)
    if path == 'img/':
        return path
    if not d.is_dir():
        raise argparse.ArgumentTypeError(f"readable_dir: {path} is not a valid path.")
    return path

def dir_file(file):
    """
    Argument parser for --inputfile option.

    Parameters
    ----------
    file : str
        Path to galaxy database file in \*.csv format.

    Returns
    -------
    pathlib.Path
        Path object of the found database file indicated by the user.

    Raises
    ------
    argparse.ArgumentTypeError
        Raised when the indicated path is not readable nor reachable.
    """
    d = Path(file)
    if file == 'config' or file == 'files/galaxies.csv':
        return file
    if not d.is_file():
        raise argparse.ArgumentTypeError(f"readable_file: {file} is not a valid file.")
    return file


INPUTCOLUMNS = ['group','galaxy','ra','dec','filename']

def readInputFile(fname:str) -> pd.DataFrame:
    """
    Reads and parses the input galaxy database file.

    Parameters
    ----------
    fname : str
        Path to galaxy database file in \*.csv format.

    Returns
    -------
    pd.DataFrame
        Pandas dataframe containing the read galaxy database file.
    """
    print(f"INFO:\tReading from '{fname}' file... ", end='', flush=True)
    df = pd.read_csv(fname,
                     converters={
                            'group': str,
                            'galaxy': str,
                            'ra': float,
                            'dec': float,
                         }
                    )
    sortby=[]
    if 'galaxy' in df.columns:
        sortby.insert(0, 'galaxy')
    if 'group' in df.columns:
        sortby.insert(0, 'group')
    df = df.sort_values(by=sortby)

    if 'filename' in df.columns:
        not_found = sum(df['filename']=='')
    else:
        not_found = 0
    #     imgpath = Path(args.path)
    #     df['filename'] = ''
    #     for i, row in df.iterrows():
    #         if 'group' in df.columns:
    #             imgfile = f'img_{row.group}_{row.galaxy}.*'
    #         else:
    #             imgfile = f'img_*_{row.galaxy}.*'
    #         image = glob.glob(f"{imgpath.absolute()}/{imgfile}")
    #         if len(image)>0:
    #             df.loc[i, 'filename'] = image[0]

    if not_found >0:
        print(f'\nWARNING: {not_found} images where not found. Check if the provided path is correct. Or download the images using the provided tool.')
    else:
        print('Done!')
    return df

def createInputFile(imgpath:str, fname:str) -> pd.DataFrame:
    """
    Creates a galaxy database file in \*.csv format for a given path containing
    images with the format "img_<group>_<galaxy>.<extension>".

    Parameters
    ----------
    imgpath : str
        Path to image folder.
    fname : str
        filename of the newly generated galaxy database.

    Returns
    -------
    pd.DataFrame
        Dataframe containing the newly generated database.
    """
    print(f'INFO:\tCreating {fname} file... ', end='', flush=True)
    df = pd.DataFrame(columns=INPUTCOLUMNS)
    if imgpath:
        for file in Path(imgpath).glob('img_*_*.*'):
            entry = {
                        'group': int(file.stem.split('_')[1]),
                        'galaxy': int(file.stem.split('_')[2]),
                        'ra':float(0),
                        'dec':float(0),
                        'filename': str(file.name),
                    }
            df = pd_concat(df, entry)
            #groups = groups.append(entry, ignore_index=True)
    # if groups not defined
    if sum(df["groups"] == '-') == len(df):
        df.drop("groups", axis=1, inplace=True)
        INPUTCOLUMNS.remove("groups")
    # sort
    sortby=[]
    if 'galaxy' in df.columns:
        sortby.insert(0, 'galaxy')
    if 'group' in df.columns:
        sortby.insert(0, 'group')
    df = df.sort_values(by=sortby)

    df.to_csv(fname, columns=INPUTCOLUMNS, index=False)
    print('Done!')
    return df


### PANDAS UTILS

COLUMNS = ["filename", "group", "galaxy", "morphology", "large", "tiny", "faceon",
           "edgeon", "star", "calibration", "recentre", "duplicated",
           "member", "hiiregion", "yes", "no", "comment", "processed",
           "fullpath", "ra", "dec"]

IDS = {
    'FILE':[],
    'TB':[],
    'CB':[],
    'RB':[],
    'RBG':[]
}

def getColumns() -> list:
    """
    Return columns

    Returns
    -------
    list
        List of columns
    """
    return COLUMNS

def getExportableColumns():
    """
    Returns the exportable columns of the program.

    Returns
    -------
    list
        List of columns (strings).
    """
    return COLUMNS[:-4]

def getRadioButtonGroups():
    """
    Returns the groups of radio buttons.

    Returns
    -------
    list
        List of groups (strings).
    """
    return IDS['RBG']

def getRadioButtonsNames():
    """
    Returns the name of radio buttons.

    Returns
    -------
    list
        List of names (strings).
    """
    return IDS['RB']

def getCheckBoxesColumns():
    """
    Returns the columns of check boxes.

    Returns
    -------
    list
        List of columns (strings).
    """
    return IDS['CB']

def getTextBoxes():
    """
    Returns the list of columns related with the text boxes.

    Returns
    -------
    list
        List of columns (strings).
    """
    return IDS['TB']


def checkColumnsMismatch(importDataColumns):
    """
    Detects if a given list of columns fits the exportable columns of this program.

    Parameters
    ----------
    importDataColumns : list
        List of columns to be checked

    Returns
    -------
    bool
        True if a mismatch is detected. False if all columns are exportable.
    """
    mismatch = False
    setID = set(importDataColumns)
    setEC = set(getExportableColumns())
    if not ( setID == setEC ):
        print('WARNING:\tColumn mismatch:')
        print("\t(-) → Missing columns in CSV:", list(setEC.difference(setID)))
        print("\t(+) → Additional columns in CSV:", list(setID.difference(setEC)), '\n')
        print("\tMaybe you are using an old savefile. Missing columns will be CREATED.")
        print("\tAdditional columns will be ERASED if you make any change.")
        mismatch = True

    return mismatch


def expand_df(selectedFiles:pd.DataFrame) -> pd.DataFrame:
    """
    Expands an existing Dataframe with the galaxy database given by the program
    arguments.

    Parameters
    ----------
    selectedFiles : pd.Dataframe
        Dataframe to be updated with the galaxy database.

    Returns
    -------
    pd.Dataframe
        Updated input dataframe.
    """
    global args
    global importData
    global groups
    if Path(args.savefile).is_file():
        importData = pd.read_csv(args.savefile,
                                 converters={
                                                'group': str,
                                                'galaxy': str,
                                            }
                                )

        if 'fits_coords' in importData.columns:
            importData['fits_coords'] = importData['fits_coords'].fillna("[]").apply(lambda x: eval(x))

        checkColumnsMismatch(importData.columns.values)
        importData['processed'] = True
        if 'filename' in groups.columns:
            importData['fullpath'] = ''
        if 'ra' in groups.columns and 'dec' in groups.columns:
            importData['ra'] = 0.0
            importData['dec'] = 0.0

        # I know there is a best way to implement it, maybe in next release
        try:
            # Saved and selected data:
            for i, row in selectedFiles.iterrows():
                item = importData.loc[importData.galaxy == row.galaxy]
                # If selected row is imported in savefile:
                if (item.size > 0):
                    if 'filename' in importData.columns:
                        file = Path(args.path) / Path(row['filename'])
                        importData.loc[importData.galaxy == row.galaxy, 'fullpath'] = file.absolute()
                    if 'ra' in importData.columns and 'dec' in importData.columns:
                        importData.loc[importData.galaxy == row.galaxy, 'ra'] = row.ra
                        importData.loc[importData.galaxy == row.galaxy, 'dec'] = row.dec
                # If selected row is not in savefile:
                else:
                    importData = pd_concat(importData, newEntry(row))

            # Add the full path to imported but unselected data:
            processedUnselectedData = importData[importData.fullpath == '']
            for i, row in processedUnselectedData.iterrows():
                if 'filename' in importData.columns:
                    file = Path(args.path) / Path(row['filename'])
                    importData.loc[importData.galaxy == row.galaxy, 'fullpath'] = file.absolute()
                if 'ra' in importData.columns and 'dec' in importData.columns:
                    ra = groups.loc[groups.galaxy == row.galaxy].ra.item()
                    dec = groups.loc[groups.galaxy == row.galaxy].dec.item()
                    importData.loc[importData.galaxy == row.galaxy, 'ra'] = ra
                    importData.loc[importData.galaxy == row.galaxy, 'dec'] = dec

        except KeyError as e:
            print(f"ERROR:\tError while parsing CSV. [{e}]")

    else:
        importData = pd.DataFrame(columns=COLUMNS)
        for i, row in selectedFiles.iterrows():
            importData = pd_concat(importData, newEntry(row))

    return importData


def newEntry(row:pd.Series) -> dict:
    """
    Generates a new entry to be inserted in the file list used by the GUI.

    Parameters
    ----------
    row : pd.Series
        Pandas Series containing the information to be inserted in the new generated entry.

    Returns
    -------
    dict
        Generated entry.
    """
    entry = {}

    if 'group' in row:
        entry.update({'group': row.group})

    if 'galaxy' in row:
        entry.update({'galaxy': row.galaxy})

    if 'filename' in row:
        file = Path(args.path) / Path(row['filename'])
        entry.update({'filename': file.name}) # default value

    if 'fits' in row:
        entry.update({'fits': row.fits})
        entry.update({'fits_coords': []})

    # ID needed? to separate widget groups?
    for i, rbgCol in enumerate(getRadioButtonGroups()):
        entry.update({rbgCol: getRadioButtonsNames()[-1]}) # default value

    for i, cbCol in enumerate(getCheckBoxesColumns()):
        entry.update({cbCol: False})

    for i, tbCol in enumerate(getTextBoxes()):
        entry.update({tbCol: ''})

    entry.update({'processed': False})

    if 'filename' in row:
        entry.update({'fullpath': file.absolute()})

    if 'ra' in row and 'dec' in row:
        entry.update({
            'ra': row.ra,
            'dec': row.dec
        })

    return entry


def save_df(df:pd.DataFrame) -> None:
    """
    Save or update the input galaxy database with a given Dataframe in a CSV format.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe to be saved.
    """
    global args
    global importData

    # Concatenate items in CSV with our processed items:
    processedItems = pd_concat(importData.loc[df['processed'] == True],
                               df.loc[df['processed'] == True])
    # processedItems = pd.concat([importData.loc[df['processed'] == True],
    #                            df.loc[df['processed'] == True]])

    # Remove old values, keep last ones:

    s_cols = ['galaxy']
    if 'group' in processedItems.columns:
        s_cols.insert(0,'group')
    exportData = processedItems.drop_duplicates(s_cols, keep='last').sort_values(by=s_cols)

    # Export final dataframe:
    exportData.to_csv(args.savefile, columns=getExportableColumns(),
                          index=False)

def pd_concat(df: pd.DataFrame, data: Union[pd.DataFrame, list, dict]) -> pd.DataFrame:
    """ Concats data to the given dataframe

    Parameters
    ----------
    df: pandas.Dataframe
        Pandas dataframe to use

    data: list, dict or pandas.Dataframe
        Data to be concatenated to the input pandas Dataframe.
        - List of the values to be concatenated (order of input values and Dataframe columns must match).
        - Dict of the 'key\:values', where keys match the Dataframe columns (if not, values are put to NaN).
        - pandas.Dataframe where columns (should) match the input Dataframe (if not, new columns are created or values are put to NaN).
    
    Returns
    -------
    pandas.DataFrame
        Dataframe containing both input Dataframes merged.
    """
    # check if data is list
    df_data = pd.DataFrame()
    if type(data) == list:
        if len(data) != len(df.columns):
            raise Exception('ERROR: Input data [list] length is not equal to input dataframe')
        df_data = pd.DataFrame([data], columns=df.columns)
    elif type(data) == dict:
        if len(data) != len(df.columns):
            warnings.warn('Input data [dict] missing input dataframe keys. Missing values inserted as NaN')
            print(list(data.keys()))
            print(list(df.columns))
        df_data = pd.DataFrame([data])
    elif type(data) == pd.DataFrame:
        if not df.empty:
            cmp_df_data = list(df.keys()[~df.keys().isin(data.keys())])
            cmp_data_df = list(data.keys()[~data.keys().isin(df.keys())])
            if len(cmp_df_data) > 0:
                warnings.warn(f'Input data missing input dataframe column. {cmp_df_data}')
            if len(cmp_data_df) > 0:
                warnings.warn(f'Input data column(s) not in input dataframe. {cmp_data_df}')
        df_data = data
    df = pd.concat([df, df_data], ignore_index=True) if len(df) > 0 else df_data
    return df
