We welcome contributions!  We use
[GitLab Flow style development](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/).
Please set up a pull request against our `main` branch with any changes
you want us to consider merging.

For more info, check the [developer guide](https://gitlab.com/astrogal/GALAssify/-/blob/main/doc/developer_guide.md)
section in our docs.
