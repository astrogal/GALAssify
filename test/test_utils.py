import os, sys
import unittest
import shutil, tempfile
from src.galassify import utils
from unittest.mock import patch
from pathlib import Path

class TestUtils(unittest.TestCase):

    #def test_get_options(self):
    #    testargs = ["main", "--list"]
    #    with patch.object(sys, 'argv', testargs):
    #        result = utils.getOptions('')
    #    print(result)

    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_exist_basic_files(self):
        os.chdir(self.test_dir)
        result = utils.exist_basic_files()
        self.assertFalse(result)

        os.mkdir('files')
        result = utils.exist_basic_files()
        self.assertFalse(result)

        Path('files/galaxies.csv').touch()
        result = utils.exist_basic_files()
        self.assertFalse(result)

        os.mkdir('img')
        result = utils.exist_basic_files()
        self.assertFalse(result)

        Path('config').touch()
        result = utils.exist_basic_files()
        self.assertTrue(result)

        # Cleanup
        os.remove('config')
        os.rmdir('img')
        os.remove('files/galaxies.csv')
        os.rmdir('files')
        os.chdir('../')


    def test_dir_path(self):
        """
        Test dir_path()
        """
        #os.mkdir("tmp")
        result = utils.dir_path('img/')
        self.assertEqual(result, 'img/')

        with self.assertRaises(utils.argparse.ArgumentTypeError):
            result = utils.dir_path('NotExists')

    def test_dir_file(self):
        """
        Test dir_file()
        """
        #os.mkdir("tmp")
        result = utils.dir_file('config')
        self.assertEqual(result, 'config')

        result = utils.dir_file('files/galaxies.csv')
        self.assertEqual(result, 'files/galaxies.csv')

        with self.assertRaises(utils.argparse.ArgumentTypeError):
            result = utils.dir_file('NotExists')

if __name__ == '__main__':
    unittest.main()
