Developer guide
===============

.. include:: ../../../developer_guide.md
   :parser: myst_parser.sphinx_
   :start-after: # Developer Guide

Contributing
------------
.. include:: ../../../../CONTRIBUTING.md
   :parser: myst_parser.sphinx_

Code of conduct
---------------

.. include:: ../../../../CODE_OF_CONDUCT.md
   :parser: myst_parser.sphinx_
