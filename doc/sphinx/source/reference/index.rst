API Reference
=============

.. toctree::
    :maxdepth: 3

    galassify
    get_images_sdss

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`