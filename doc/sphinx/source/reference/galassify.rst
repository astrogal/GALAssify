GALAssify package
=================

.. automodule:: galassify
    :members:
    :show-inheritance:
    :inherited-members:

Submodules
----------

.. toctree::
    :maxdepth: 2

    galassify.galassify
    galassify.gui
    galassify.utils
    galassify.widgets